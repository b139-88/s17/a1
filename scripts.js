let students = [];

function addStudent(studentName){
    if (studentName != undefined || studentName != null || studentName != '') {
        students.push(studentName);
        console.log(`${studentName} was added to the student's list`);
    }
}

addStudent('Raymond');
addStudent('John');
addStudent('Jane');
addStudent('Jane');
addStudent('Joe');

//Parameter will be added in functions for the function to be dynamic and not rely on global variables

function countStudents(){
    console.log(`There are a total of ${students.length} students enrolled.`);
}

countStudents();

function printStudents(){
    students.sort();

    students.forEach((student) => {
        console.log(`${student}`);
    })
}

printStudents();

function findStudent(studentName){
    let studentsFound = students.filter(student => {
        return student.toLowerCase() === studentName.toLowerCase();
    });

    if (studentsFound.length == 1) {
        console.log(`${studentsFound[0]} is an enrollee.`);
    } else if (studentsFound.length >= 2) {
        console.log(`${studentsFound} are enrollees.`);
    } else {
        console.log(`${studentName} is not an enrollee.`);
    }
}

findStudent('raymond');
findStudent('jane');
findStudent('Mark');

function addSection(section){
    let studentWithSection = students.map((student) => {
        return [student, ` - Section ${section}`].join(' ');
    });

    return studentWithSection;
}

console.log(addSection('A'));

function removeStudent(studentName){
    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1);
    }

    studentName = capitalize(studentName);

    let studentsFound = students.indexOf(studentName);

    students.splice(studentsFound, 1);

    console.log(`${studentName} was removed from the student's list.`);
}

removeStudent('joe');


printStudents();

